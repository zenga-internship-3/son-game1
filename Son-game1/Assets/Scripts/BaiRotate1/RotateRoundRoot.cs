﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateRoundRoot : MonoBehaviour
{
    [SerializeField] float m_MinCamY = -50;
    [SerializeField] float m_MaxCamY = 50;
    Transform m_CameraComp;
    Vector3 m_PrevMousePosition = Vector3.zero;
    Vector3 m_NextRay = Vector3.zero;
    Vector3 m_MousePosition = Vector3.zero;
    Vector3 m_Rotation = Vector3.zero;
    Ray ray;
    float m_SignedAngle;
    float m_Y;

    private void Awake()
    {
        m_CameraComp = Camera.main.transform;
        m_CameraComp.transform.rotation = Quaternion.identity;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            m_PrevMousePosition = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            var p1 =  Camera.main.ScreenToWorldPoint(new Vector3(m_PrevMousePosition.x, m_PrevMousePosition.y, 1f)) - m_CameraComp.position;
            var p2 = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1f)) - m_CameraComp.position;
            var angle = Vector3.SignedAngle(p1, p2, Vector3.up);
            m_CameraComp.Rotate(0, -angle, 0, Space.World);
            if (m_CameraComp.eulerAngles.y > m_MaxCamY)
            {
                m_CameraComp.eulerAngles = new Vector3(m_CameraComp.localEulerAngles.x, m_MaxCamY, m_CameraComp.localEulerAngles.z);
            }
            else if (m_CameraComp.eulerAngles.y < m_MinCamY)
            {
                m_CameraComp.eulerAngles = new Vector3(m_CameraComp.localEulerAngles.x, m_MinCamY, m_CameraComp.localEulerAngles.z);
            }
            m_PrevMousePosition = Input.mousePosition;
            Debug.Log(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1f)));
        }
    }
}
