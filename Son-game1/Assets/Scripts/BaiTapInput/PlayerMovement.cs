﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    void FixedUpdate()
    {
        MoveFollowRay();
    }

    private void MoveFollowRay()
    {
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100f, ~(1 << 9), QueryTriggerInteraction.Ignore))
            {
                Vector3 offset = new Vector3(hit.point.x,0f, hit.point.z);
                 this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition,hit.point, 0.3f);
            }
            Debug.DrawRay(ray.origin, ray.direction * 100f, Color.green);
        }
    }
}
