﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class MoveByClick : MonoBehaviour
{
    public enum State {RigidMovePosition,RigidVelocity,TransformPosition,TransformTranslate};
    [SerializeField] Transform m_Object;
    [SerializeField] Transform m_Target;
    [SerializeField] State m_State;
    [SerializeField] float m_Speed = 2f;
    Rigidbody m_ObjectRigid;
    BoxCollider m_ObjectBox;
    Vector3 m_Pos;
    Vector3 m_FirstPos;
    Vector3 m_Distant;
    float time;

    private void Awake()
    {
        m_ObjectRigid = m_Object.GetComponent<Rigidbody>();
        m_ObjectBox = m_Object.GetComponent<BoxCollider>();
    }

    //Speed: 2 unit per second
    void Update()
    {
        Ray ray;
        RaycastHit hit;
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 300f, 1 << 9, QueryTriggerInteraction.Collide))
        {
            if (Input.GetMouseButton(0))
            {
                if (m_Target == null)
                {
                    m_Target = hit.collider.transform;
                    m_Pos = m_Target.position - m_Object.position;
                    m_FirstPos = m_Object.position;
                }
                m_Distant = m_Target.position - m_Object.position;
                //if (m_Distant.magnitude >= m_ObjectBox.bounds.size.magnitude)
                //{
                if (m_State == State.RigidMovePosition)
                {
                    m_ObjectRigid.MovePosition(m_ObjectRigid.position + m_Pos.normalized*m_Speed*Time.deltaTime);
                }
                else if (m_State == State.RigidVelocity)
                {
                    m_ObjectRigid.velocity = m_Pos.normalized * m_Speed;
                }
                else if (m_State == State.TransformPosition)
                {
                    m_Object.position += m_Pos.normalized * m_Speed*Time.deltaTime;
                }
                else if (m_State == State.TransformTranslate)
                {
                    m_Object.Translate(m_Pos.normalized * m_Speed*Time.deltaTime);
                }
                //}
            }
            else m_Target = null;
        }

        Debug.DrawRay(ray.origin, ray.direction * 100, Color.green);
    }
    void ReLoad()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void OnGUI()
    {
        if(GUILayout.Button("Reset"))
        {
            ReLoad();
        }
    }
}


